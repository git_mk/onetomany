package com.example.jpa.jpaOneToManyDemo.repository;

import com.example.jpa.jpaOneToManyDemo.model.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostRepository extends JpaRepository<Post,Long> {
}
